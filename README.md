# MCP Testing Guide for Sr. Java Developer

The following repo contains test.

## Introduction

This is test for the position of *Sr. Java Developer*  in MC Payment.
I completed this test with some steps as defined in this document:

1. I created Restful API for **Third Party Payment System** application.
2. Clone this repository and I updated the code of **Test-Java** application
3. I created **Service Registry** to connect both application as a Microservices.
